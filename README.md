OpauthConnect
=============

Signing in via social networks.  
Plugin for [esoTalk](http://esotalk.org) forum engine.

Supported services
------------------

1. Google plus
2. Facebook
3. Twitter

Steps to install
----------------

1. Download and extract plugin
2. Rename plugin folder to `OpauthConnect` (case sensitive!)
3. Move plugin to esoTalk plugin directory (in newest version it is "*addons/plugins*")
4. Enable plugin on esoTalk administration page
5. Check needed social networks and enter credentials
6. Go to your Google/Facebook/Twitter application settings and add callback URL:  
For Facebook: Fill "*Site URL*" field with `http://yourforum.com/user/social/facebook`  
For Google: Fill "*Authorized redirect URI*" field with `http://yourforum.com/user/social/google/oauth2callback`  
For Twitter: Fill "*Callback URL*" field with `http://yourforum.com/user/social/twitter/oauth_callback`
7. Add following code where you want to display login buttons  
`<?php $this->trigger("RenderOpauth"); ?>`

Steps to update to version 2.0
------------------------------

*One note before we start. After updating you will lose some information about users, but no worries. 
This will not affect Google and Facebook users (they just need to press "complete authorization" button after first login attempt), 
but Twitter users have to enter and confirm their email address once more.*

1. Backup your database (**Recommended**)
2. Don't forget to backup your custom templates (if any)
3. Update your config file "*config/config.php*"  
There is no way to update it automatically, so you have to do it by yourself.  
By default there is no writing permission to config file. You have to add it first.  
Find lines started with "*plugin.opauthconnect*" and replace its prefix "*plugin.opauthconnect*" to `OpauthConnect`
4. Do needed steps to install

Customizing templates
---------------------

*No need to edit templates directly in plugin directory and then taking care about your templates after update. 
You can overwrite needed templates in your skin. 
You can overwrite any template. But I would recommend to customize only **emails** and **login buttons** templates.  
Here are some steps:*

1. Take a look on views structure in plugin folder and find needed template
2. Go to your skin views directory "*addons/skins/yourskinname/views*". If you are not using custom skin you can choose "*Default*", but 
in this case be careful with next esoTalk updates.
3. Add needed template to skin views folder.  
For example, your need to customize login buttons. So the filepath is `social/oc_buttons.php`. 
You have to create folder `social` in your skin views directory, then add file `oc_buttons.php` to newly created folder.  
***Done!*** Now plugin is using your template instead of original.
4. To be aware of what variables you can use in your custom templates, take a look on comments in original plugin templates